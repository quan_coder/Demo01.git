package com.xzy.bean;
/**
 * spring容器的继承
 * @author 全文超
 * 2016-11-22 18:24:51
 *
 */
public class Father {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
